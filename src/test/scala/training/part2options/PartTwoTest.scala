package training.part2options

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PartTwoTest extends AnyFlatSpec with Matchers {

  "PartTwo sum methods" should "sum revenue and tax values" in {
    PartTwo.sumWithGetOrElse(Some(10d), Some(5d)) shouldEqual 15d
    PartTwo.sumWithPatternMatching(Some(10d), Some(5d)) shouldEqual 15d
    PartTwo.sumWithGetOrElse(Some(10d), None) shouldEqual 10d
    PartTwo.sumWithPatternMatching(Some(10d), None) shouldEqual 10d
    PartTwo.sumWithGetOrElse(None, Some(10d)) shouldEqual 10d
    PartTwo.sumWithPatternMatching(None, Some(10d)) shouldEqual 10d
    PartTwo.sumWithGetOrElse(None, None) shouldEqual 0d
    PartTwo.sumWithPatternMatching(None, None) shouldEqual 0d
  }
}
