package training.part8patternMatchingAdv

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AdvancedPatternMatchingTest extends AnyFlatSpec with Matchers {

  "checkIfHasExtraOnion method" should "return string which describes if burger has extra onion" in {
    val burgerWithExtraOnion = Burger("onionBurger", extraOnion = true)
    val withoutExtraOnion = Burger("onionBurger", extraOnion = false)


    BurgerPatternMatching.checkIfHasExtraOnion(burgerWithExtraOnion) shouldBe "Ordered with extra onion"
    BurgerPatternMatching.checkIfHasExtraOnion(withoutExtraOnion) shouldBe "Without extra onion"
  }

  "getBurgerType method" should "return standard if burger name is hamburger, otherwise return custom" in {
    val standardBurger = Burger("hamburger", extraOnion = true)
    val customBurger = Burger("some burger", extraOnion = false)


    BurgerPatternMatching.getBurgerType(standardBurger.name) shouldBe "standard"
    BurgerPatternMatching.getBurgerType(customBurger.name) shouldBe "custom"
  }

  "matchNumber method" should "describe the number" in {
    NumberMatcher.matchNumber(12) shouldBe "Greater then 10"
    NumberMatcher.matchNumber(10) shouldBe "Less or equal to 10 and not equal to five"
    NumberMatcher.matchNumber(5) shouldBe "Equal to five"
  }

}
