package training.part6implicits

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CarStringifierTest extends AnyFlatSpec with Matchers {

  "optionStringToString" should "properly convert string" in {
    ???
  }

  "optionStringToString" should "return Unknown when None" in {
    ???
  }

  "optionIntToString" should "properly convert integer" in {
    ???
  }

  "optionDoubleToString" should "properly convert double" in {
    ???
  }

  "optionBooleanToString" should "properly convert boolean" in {
    ???
  }

  val car: Car = Car("Renault", Some("Captur"), Some(2017), Some(1.2), None)
  val carStringified: CarStringified = CarStringified("Renault", "Captur", "2017", "1.2", "Unknown")

  "CarStringifier" should "properly convert class" in {
    CarStringifier.stringify(car) shouldEqual carStringified
  }
}