package training.part7currying

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CurryingTest extends AnyFlatSpec with Matchers {

  "Curried is Between method" should "return correct numbers" in {
    Currying.isBetween10and30(20) shouldBe true
    Currying.isBetween10and30(50) shouldBe false
  }

  it should "calculate years between date and year 2020" in {
    Currying.calculateYearsTo2020(1997) shouldBe 23
  }

}
