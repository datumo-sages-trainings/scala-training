package training.part5errorHandling

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.util.Success

class ErrorHandlingTest extends AnyFlatSpec with Matchers {

  "toIntOption method" should "return Some()" in {
    ErrorHandling.toIntOption("123") shouldBe Some(123)
  }

  it should "return None for incorrect int" in {
    assertResult(None)(ErrorHandling.toIntOption("it is not int type"))
  }


  "toIntTry method" should "return Success" in {
    ErrorHandling.toIntTry("123") shouldBe Success(123)
  }

  it should "return Failure for incorrect int" in {
    ErrorHandling.toIntTry("it is not int type") should not be Success
  }


  "toIntEither method" should "return Right" in {
    ErrorHandling.toIntEither("123") shouldBe Right(123)
  }

  it should "return Failure for incorrect int" in {
    assertResult(Left("it is not int type"))(ErrorHandling.toIntEither("it is not int type"))
  }
}
