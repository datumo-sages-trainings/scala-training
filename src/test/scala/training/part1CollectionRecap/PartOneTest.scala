package training.part1CollectionRecap

import org.scalatest.flatspec.AnyFlatSpec

class PartOneTest extends AnyFlatSpec {

  "PartOne" should "converts all strings to their upper case" in {
    assertResult(List("AAA", "BB", "C"))(PartOne.upperCase(List("aaa", "bb", "c")))
    assertResult(List("QWERTY"))(PartOne.upperCase(List("qWeRtY")))
  }


  it should "counts all the string which has \"a\" letter inside" in {
    assertResult(3)(PartOne.countStringsWithA(List("Ala", "ma", "kota")))
    assertResult(1)(PartOne.countStringsWithA(List("aaa", "bb", "c", "dddd")))
  }

  it should "removes duplicates and sum it into long" in {
    assertResult(283)(PartOne.removeDuplicatesAndSum(List(234, 23, 6, 1, 4, 12, 6, 3, 3, 23)))
    assertResult(10)(PartOne.removeDuplicatesAndSum(List(1, 1, 2, 2, 2, 3, 4, 4)))
  }

  it should "test if a map contains a mapping for the specified key" in {
    val map = Map(32 -> 2, 654 -> 1, 546 -> 5, 12 -> 4)
    assertResult(true)(PartOne.mapContainsKey(map, 654))
    assertResult(true)(PartOne.mapContainsKey(map, 12))
    assertResult(false)(PartOne.mapContainsKey(map, 123))
    assertResult(false)(PartOne.mapContainsKey(map, 99))
  }

  it should "calculate mean" in {
    assertResult(6.5)(PartOne.mean(List(1, 2, 4, 6, 12, 14)))
  }
}
