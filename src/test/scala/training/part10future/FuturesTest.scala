package training.part10future

import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Span}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}
import scala.util.{Failure, Success}

class FuturesTest extends AnyFlatSpec with Matchers {

  object TestObject

  "extractTry" should "wrap successful futures in a Success" in {
    Futures.extractTry(Future.successful(TestObject)).futureValue shouldBe Success(TestObject)
  }

  it should "catch errors and wrap them in a Failure" in {
    val darn = new RuntimeException("Darn")
    Futures.extractTry(Future.failed(darn)).futureValue shouldBe Failure(darn)
  }

  "inParallel" should "run the provided futures in parallel" in {
    def slowNumber(n: Int) = Future {
      blocking {
        Thread.sleep(100)
        n
      }
    }

    val r = Futures.runInParallel(slowNumber(1), slowNumber(2))

    r.isReadyWithin(Span(110, Millis))
    r.futureValue should be((1, 2))
  }

  "collectSuccessful" should "collect the futures that succeed" in {
    Futures.collectSuccessful(Seq(
      Future.successful(1),
      Future.failed(new RuntimeException("oops")),
      Future.successful(1),
      Future.successful(2),
      Future.failed(new RuntimeException("oops")),
      Future.successful(3)
    )).futureValue shouldBe Seq(1, 1, 2, 3)
  }
}