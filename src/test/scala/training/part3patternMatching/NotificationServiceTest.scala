package training.part3patternMatching

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class NotificationServiceTest extends AnyFlatSpec with Matchers {

  "NotificationService notifyDevice method" should "send pushNotification when notifying Phone with osVersion >= 5" in {
    val samsungS9 = Phone("1", 9)

    NotificationService.notifyDevice(samsungS9) shouldBe PushNotification(samsungS9.deviceId)
  }

  it should "send smsNotification when notifying Phone with osVersion < 5" in {
    val samsungS3 = Phone("2", 4)
    NotificationService.notifyDevice(samsungS3) shouldBe SmsNotification(samsungS3.deviceId)
  }

  it should "send emailNotification when notifying Computer" in {
    val pc = Computer("3", "dev@datumo.pl")
    NotificationService.notifyDevice(pc) shouldBe EmailNotification(pc.email)
  }

  it should "send xboxNotification when notifying Xbox" in {
    val xbox = Xbox("4", "ninja")
    NotificationService.notifyDevice(xbox) shouldBe XboxLiveNotification(xbox.xboxLiveId)
  }
}
