package training.part9typeClass

object TypeClasses extends App {
  /*
  1. define a trait with method "transform"
  2. define an object that extends this trait and takes parameter of generic type and implicit of defined trait type
  3. write 3 implicit objects with defined transform method, which will perform following operations:
   - for string, take first five characters. If there are less then five chars, left pad it with empty spaces
   - for double multiply it by 5
   - for int return squared value
  4. write a tests for for created object from point 2 when given string, double or int type
  5. Write an implicit classes for string, double and int which performs the same operations as in point 3.
   Use them in implicit object.
   */
}


