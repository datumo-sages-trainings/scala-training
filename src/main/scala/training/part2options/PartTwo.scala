package training.part2options

object PartTwo {

  //use get or else
  def sumWithGetOrElse(revenue: Option[Double], tax: Option[Double]): Double = {
    ???
  }

  //use pattern matching
  def sumWithPatternMatching(revenue: Option[Double], tax: Option[Double]): Double = {
    ???
  }
}
