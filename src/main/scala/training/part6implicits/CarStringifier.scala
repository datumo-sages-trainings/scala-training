package training.part6implicits

case class Car(
              brand: String,
              model: Option[String],
              productionYear: Option[Int],
              engineDisplacement: Option[Double],
              isRunning: Option[Boolean]
              )

case class CarStringified(
                brand: String,
                model: String,
                productionYear: String,
                engineDisplacement: String,
                isRunning: String
              )

object CarStringifier {
  // Stringify method should convert from Cat to CarStringified
  // Use import implicits here

  def stringify(car: Car): CarStringified = {
    ???
  }
}
