package training.part7currying

object Currying {
  def isBetween(min: Double, max: Double, value: Double): Boolean = ???

  def yearsBetween(endYear: Int, startYear: Int):Int = ???

  def isBetweenCurried: Double => Double => Double => Boolean = ???
  def isBetween10and30: Double => Boolean = ???

  def yearsBetweenCurried: Int => Int => Int = ???
  def calculateYearsTo2020: Int => Int = ???

}