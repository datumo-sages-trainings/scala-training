package training.part1CollectionRecap

import scala.collection.immutable.{List, Map}

object PartOne {

  //exercise 1
  //Write a method that converts all strings in a list to their upper case.
  def upperCase(list: List[String]): List[String] = list.map(_.toUpperCase)

  //exercise 2
  // Write a method that counts all the string which has "a" letter inside
  def countStringsWithA(list: List[String]): Long = list.count(_.contains("a"))

  //exercise 3
  // Write a method that removes duplicates and sum it into long
  def removeDuplicatesAndSum(list: List[Int]): Long = list.toSet.sum

  //exercise 4
  //test if a map contains a mapping for the specified key
  def mapContainsKey(map: Map[Int, Int], key: Int): Boolean = map.exists(_._1 == key)

  //exercise 5
  // write method which returns mean value of all elements of the list
  def mean(seq: Seq[Int]): Double = {
    seq.sum.toDouble / seq.length.toDouble
  }
}
