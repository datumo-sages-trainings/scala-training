package training.part10future

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

object Futures {

  def extractTry[A](future: Future[A]): Future[Try[A]] = ???

  def collectSuccessful[A](futures: Seq[Future[A]]): Future[Seq[A]] = ???

  def runInParallel[A, B](f1: => Future[A], f2: => Future[B]): Future[(A, B)] = ???
}

