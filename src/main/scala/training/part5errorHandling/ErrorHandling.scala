package training.part5errorHandling

import scala.util.Try

object ErrorHandling extends App {

  def toIntOption(s: String): Option[Int] = ???

  def toIntTry(s: String): Try[Int] = ???

  def toIntEither(s: String): Either[String, Int] = ???
}
