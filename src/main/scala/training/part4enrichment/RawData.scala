package training.part4enrichment

case class RawData(
                    id: String,
                    name: String,
                    originalValue: Double,
                    isOnSale: Option[Boolean],
                    salePercentage: Option[Double]
                  )
