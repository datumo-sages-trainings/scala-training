package training.part4enrichment

case class EnrichedData(
                         id: String,
                         name: String,
                         valueAfterSale: Double,
                         isOnSale: Boolean
                       )

/* 1. Fill apply, convertToBoolean and valueAfterSale methods.
   2. Convert to boolean should return false, if value is not defined.
   3. If salesPercentage is not defined, you should assume that it is not on sale.

   4. Value after sale should lower the original value by sale amount (if it is on sale, otherwise return original value)
   5. Apply method in EnrichedData object should transform RawData to Enriched using
   convertToBoolean and valueAfterSale.
   6. Write unit tests for all three methods.
 */
object EnrichedData {
  def apply(r: RawData): EnrichedData = {

    EnrichedData(
      ???
    )
  }

  def convertToBoolean(predicate: Option[Boolean]): Boolean = {
    ???
  }

  def valueAfterSale(value: Double, isOnSale: Boolean, salesPercentage: Option[Double]): Double = {
    ???
  }
}
