package training.part3patternMatching

sealed abstract class Device
case class Phone(deviceId: String, appVersion: Int) extends Device
case class Computer(deviceId: String, email: String) extends Device
case class Xbox(deviceId: String, xboxLiveId: String) extends Device

sealed abstract class Notification
case class PushNotification(deviceId: String) extends Notification
case class EmailNotification(email: String) extends Notification
case class SmsNotification(deviceId: String) extends Notification
case class XboxLiveNotification(xboxLiveId: String) extends Notification


object NotificationService {
  def notifyDevice(device: Device): Notification = {
    ???
  }
}
