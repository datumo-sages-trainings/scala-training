package training.part8patternMatchingAdv

case class Burger(name: String, extraOnion: Boolean)

object Burger {
  ???
}

object BurgerPatternMatching {
  def checkIfHasExtraOnion(burger: Burger): String = ???

  def getBurgerType(burgerType: String): String = ???
}

object GreaterThen10 {???}

object EqualToFive {???}

object NumberMatcher {
  def matchNumber(value: Int): String = ???
}
