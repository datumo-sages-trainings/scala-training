name := "scala-training"

version := "1.0"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "joda-time" % "joda-time" % "2.10.5",
  "org.scalatest" %% "scalatest" % "3.1.0" % "test"
)
